import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] arr = new int[n][n];
        for(int i=0;i<n;i++)
        {

            for(int j=0;j<n;j++)
            {
                arr[i][j]=in.nextInt();
            }
        }
        in.close();
        int leftToRight=0;
        int rightToLeft=0;
        for(int i=0;i<n;i++)
        {
                leftToRight+=arr[i][i];
                rightToLeft+=arr[i][n-i-1];
        }
        int result = Math.abs(leftToRight-rightToLeft);
        System.out.println(result);
    }
}