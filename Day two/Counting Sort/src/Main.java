import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int i=0;i<n;i++)
            arr[i]=in.nextInt();
        in.close();
        int max= arr[0];
        for(int i: arr){
            if(i>max)
                max=i;
        }
        //System.out.println(max);
        int[] count = new int[100];
        for(int i=0;i<arr.length;i++)
        {
            count[arr[i]]++;
        }

        for(int i=0;i<100;i++)
        {
            System.out.print(count[i]+" ");
        }

    }
}