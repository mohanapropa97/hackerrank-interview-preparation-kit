import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int i =0 ;i<n; i++)
            arr[i] = in.nextInt();
        in.close();
        int uniqueElement = 0;

        for (int num : arr) {
            uniqueElement ^= num;
            //System.out.println("Result: "+uniqueElement+" num: "+ num);
        }
        System.out.println(uniqueElement);
    }
}