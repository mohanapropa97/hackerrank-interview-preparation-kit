public class Result {
    public static String timeConversion(String s) {
      String[] timeFormat = s.split(":");

      int hh = Integer.parseInt(timeFormat[0]);
      int mm = Integer.parseInt(timeFormat[1]);
      int ss = Integer.parseInt(timeFormat[2].substring(0,2));
      String meridiem= timeFormat[2].substring(2);

        if (meridiem.equals("PM") && hh != 12) {
            hh += 12;
        } else if (meridiem.equals("AM") && hh == 12) {
            hh = 0;
        }

        return String.format("%02d:%02d:%02d", hh, mm, ss);

    }
}