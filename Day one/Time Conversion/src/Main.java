import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String inputTime = in.nextLine();
        in.close();
        String outputTime = Result.timeConversion(inputTime);
        System.out.println(outputTime);
    }
}