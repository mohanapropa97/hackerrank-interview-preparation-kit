import java.util.List;

public class Result {

    public static void plusMinus(List<Integer> arr, int n) {
        double pos=0,neg=0,zero=0;
        for(int i : arr)
        {
            if(i>0)
                pos++;
            else if(i<0)
                neg++;
            else
                zero++;
        }

        System.out.println(pos/ n);
        System.out.println(neg/ n);
        System.out.println(zero/ n);

    }

}
